package com.example.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    Button btnPrevius, btnDelete;

    ArrayDeque<String> texts = new ArrayDeque<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.editText);
        btnPrevius = findViewById(R.id.btnPrev);
        btnDelete = findViewById(R.id.btnDelete);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.toString().equals("")) {
                    editText.setText("Поле не может быть пустым!");
                }
                else {
                    texts.addFirst(editText.getText().toString());
                    editText.setText("");
                }
            }
        });
        btnPrevius.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(texts.pollFirst());
            }
        });
    }
}
